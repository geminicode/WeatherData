package edu.gtri.weather;

import java.util.Calendar;
import java.util.Date;

/**
 * Structure for the Average Dry-Bulb Temperature between 
 * sunrise and sunset for a given date.
 * 
 * @author Darrell Fuller
 *
 */
public class AverageDaytimeTemperature {
	
	/**
	 * Date is immutable
	 */
	private Date date;
	private Date sunrise;
	private Date sunset;
	private int averageTemp;
	private double stdDevTemp;
	private String stationName;
	
	public AverageDaytimeTemperature() 
	{
		this(null);
	}
	public AverageDaytimeTemperature(java.util.Date date) 
	{
		Calendar cal = Calendar.getInstance();
		if (date != null)  // If date is null will use the current day
			cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.date = cal.getTime();
	}
	public Date getDate() {
		return date;
	}
	public Date getSunrise() {
		return sunrise;
	}
	public void setSunrise(Date sunrise) {
		this.sunrise = sunrise;
	}
	public Date getSunset() {
		return sunset;
	}
	public void setSunset(Date sunset) {
		this.sunset = sunset;
	}
	public int getAverageTemp() {
		return averageTemp;
	}
	public void setAverageTemp(int averageTemp) {
		this.averageTemp = averageTemp;
	}
	public double getStdDevTemp() {
		return stdDevTemp;
	}
	public void setStdDevTemp(double stdDevTemp) {
		this.stdDevTemp = stdDevTemp;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	@Override
	public String toString() {
		return String.format("StationName : %s\n"
				+ "Date : %s\n"
				+ "Sunrise : %s\n"
				+ "Sunset : %s\n"
				+ "Average Temp : %s\n"
				+ "Standard Deviation  : %s\n"
				+ "---------------\n\n", 
				getStationName(),
				getDate(),
				getSunrise(),
				getSunset(),
				getAverageTemp(),
				getStdDevTemp()
				);
	}
	
}
