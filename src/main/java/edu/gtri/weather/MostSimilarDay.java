package edu.gtri.weather;

import java.util.Collection;
import java.util.Date;

public class MostSimilarDay {

	Date date;
	Collection<Integer> texasTemps;
	Collection<Integer> georgiaTemps;
	private int averageTexasTemp;
	private int averageGeorgiaTemp;
	double similarity = 0.0d;
	
	public Collection<Integer> getTexasTemps() {
		return texasTemps;
	}
	public void setTexasTemps(Collection<Integer> texasTemps) {
		this.texasTemps = texasTemps;
	}
	public Collection<Integer> getGeorgiaTemps() {
		return georgiaTemps;
	}
	public void setGeorgiaTemps(Collection<Integer> georgiaTemps) {
		this.georgiaTemps = georgiaTemps;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getAverageTexasTemp() {
		return averageTexasTemp;
	}
	public void setAverageTexasTemp(int averageTexasTemp) {
		this.averageTexasTemp = averageTexasTemp;
	}
	public int getAverageGeorgiaTemp() {
		return averageGeorgiaTemp;
	}
	public void setAverageGeorgiaTemp(int averageGeorgiaTemp) {
		this.averageGeorgiaTemp = averageGeorgiaTemp;
	}
	@Override
	public String toString() {
		return String.format("Date : %s\n"
				+ "Texas Average Temp : %s\n"
				+ "Georgia Average Temp : %s\n"
				+ "Similarity : %s\n"
				+ "Texas Dataset : %s\n"
				+ "Georgia Dataset  : %s\n"
				+ "---------------\n\n", 
				getDate(),
				getAverageTexasTemp(),
				getAverageGeorgiaTemp(),
				getSimilarity(),
				getTexasTemps(),
				getGeorgiaTemps()
				);
	}

}
