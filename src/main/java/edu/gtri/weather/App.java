package edu.gtri.weather;

import java.util.Calendar;

/**
 * WeatherData Main Application
 *
 */
public class App 
{
    public static void main( String[] args ) 
    	throws Exception
    {
        App app = new App();
        System.out.println( "Question 1. Solution\n" );
        app.runGetAverageDaytimeTemperature();
        System.out.println( "Question 2. Solution\n" );
        app.runGetWindChillWhenTempBelow40();
        System.out.println( "Question 3. Solution\n" );
        app.runGetMostSimilarDay();
   }
    
	public void runGetAverageDaytimeTemperature() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		Calendar cal = Calendar.getInstance();
		cal.set(2017,Calendar.FEBRUARY,1);
		AverageDaytimeTemperature results = reader.getAverageDaytimeTemperature(cal.getTime());
		System.out.print(results.toString());
	}
   
	public void runGetWindChillWhenTempBelow40() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		Calendar cal = Calendar.getInstance();
		cal.set(2017,Calendar.FEBRUARY,1);
		WindChillWhenBelow40 results = reader.getWindChillWhenTempBelow40(cal.getTime());
		System.out.print(results.toString());
	}	
	
	public void runGetMostSimilarDay() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		MostSimilarDay results = reader.getMostSimilarDay();
		System.out.print(results.toString());
	}	
	
}
