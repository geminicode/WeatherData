package edu.gtri.weather;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Structure for the Average Wind Chill Factor between 
 * sunrise and sunset for a given date.
 * 
 * @author Darrell Fuller
 *
 */
public class WindChillWhenBelow40 {
	
	/**
	 * Date is immutable
	 */
	private Date date;
	private String stationName;
	private int averageTemp;
	private int averageWindChill;
	private int averageWind;
	
	Map<Date,Integer> results;
	
	public WindChillWhenBelow40() 
	{
		this(null);
	}
	public WindChillWhenBelow40(java.util.Date date) 
	{
		Calendar cal = Calendar.getInstance();
		if (date != null)  // If date is null will use the current day
			cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.date = cal.getTime();
	}
	public Date getDate() {
		return date;
	}

	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	public Map<Date, Integer> getResults() {
		return results;
	}
	public void setResults(Map<Date, Integer> results) {
		this.results = results;
	}
	
	public void addResults(Date date, Integer windChill) {
		if (this.results == null)
			this.results = new HashMap<Date, Integer>();
		this.results.put(date, windChill);
	}
	
	public int getAverageTemp() {
		return averageTemp;
	}
	public void setAverageTemp(int averageTemp) {
		this.averageTemp = averageTemp;
	}
	public int getAverageWindChill() {
		return averageWindChill;
	}
	public void setAverageWindChill(int averageWindChill) {
		this.averageWindChill = averageWindChill;
	}
	public int getAverageWind() {
		return averageWind;
	}
	public void setAverageWind(int averageWind) {
		this.averageWind = averageWind;
	}
	
	private String formatResult()
	{
		StringBuffer buffer = new StringBuffer();
		SimpleDateFormat formatter = new SimpleDateFormat("kk:mm");
		for (Date key : results.keySet() )
		{
			String item = String.format("%s=%s ", formatter.format(key), results.get(key));
			buffer.append(item);
		}
		return buffer.toString().trim().replace(" ", ", ");
	}
	
	@Override
	public String toString() {
		return String.format("StationName : %s\n"
				+ "Date : %s\n"
				+ "Average Temp : %s\n"
				+ "Average Wind Speed : %s\n"
				+ "Average Wind Chill : %s\n"
				+ "Results : [%s]\n"
				+ "---------------\n\n", 
				getStationName(),
				getDate(),
				getAverageTemp(),
				getAverageWind(),
				getAverageWindChill(),
				formatResult()
				);
	}

}
