package edu.gtri.weather;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class Utils {

	/**
	 * Convert a String "1/1/08 0:52" into a Date
	 * @param dateString
	 * @return
	 * @throws ParseException 
	 */
	public static Date getDate(String dateString) throws ParseException
	{
		Date date = new SimpleDateFormat("M/d/yy kk:mm").parse(dateString);
		return date;
	}

	public static Date getDateByHour(String dateString) throws ParseException
	{
		Date date = getDate(dateString);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static Date getZeroDate(String dateString) throws ParseException
	{
		Date date = getDate(dateString);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}

	public static Date getZeroDate(Date date) throws ParseException
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static Date getDate(Date date, String timeString) throws ParseException
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formattedTimeString = String.format("%s:%s", timeString.substring(0, timeString.length()-2), timeString.substring(timeString.length()-2));
		String dateString = String.format("%s/%s/%s %s", cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE), cal.get(Calendar.YEAR), formattedTimeString);
		Date newdate = getDate(dateString);
		return newdate;
	}

	public static int average(Collection<Integer> items) {
	      int sum = 0;
	      Integer[] ints = items.toArray(new Integer[0]);
	      for (int i=0; i< ints.length; i++) {
	    	  sum += ints[i];
	      }
	      return sum / ints.length;
	      /*
	      for (int i=0; i< items.size(); i++) {
	    	  if (items instanceof List)
	            sum += ((List<Integer>)items).get(i);
	    	  if (items instanceof Set)
		            sum += ((Set<Integer>)items).get(i);
	      }
	      return sum / items.size();
	      */
	}

	public static double stdDev(List<Integer> items) {
		  int average = average(items);
	      double sum = 0;
	      for (int i=0; i<items.size();i++)
	      {
	    	  sum = sum + (Math.pow(items.get(i) - average, 2)/items.size());
	      }
	      double stdDev= Math.sqrt(sum);
	      return stdDev;
	}
	
	public static int windChill(int temp, int velocity)
	{
		double chill = 35.74 + (0.6215 * temp) - (35.75 * Math.pow(velocity, 0.16)) + (0.4275 * temp * Math.pow(velocity, 0.16));
		return (int) Math.round(chill);
	}

	public static int getInt(String value) {
		return Integer.parseInt(value);
	}
}
