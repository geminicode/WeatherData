package edu.gtri.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.text.similarity.CosineSimilarity;

public class WeatherFileReader {

	public static final String DATA_FILE_CANADIAN_TX = "/1089419.csv";
	public static final String DATA_FILE_WBAN_GA = "/1089441.csv";
	
	private Logger log = Logger.getLogger(getClass().getName());
	private enum HEADERS {
		STATION, 
		STATION_NAME, 
		ELEVATION, 
		LATITUDE, 
		LONGITUDE, 
		DATE, 
		REPORTTPYE, 
		HOURLYSKYCONDITIONS, 
		HOURLYVISIBILITY, 
		HOURLYPRSENTWEATHERTYPE, 
		HOURLYDRYBULBTEMPF,   // Question1 
		HOURLYDRYBULBTEMPC, 
		HOURLYWETBULBTEMPF, 
		HOURLYWETBULBTEMPC, 
		HOURLYDewPointTempF, 
		HOURLYDewPointTempC, 
		HOURLYRelativeHumidity, 
		HOURLYWindSpeed,    // Question2
		HOURLYWindDirection, 
		HOURLYWindGustSpeed, 
		HOURLYStationPressure, 
		HOURLYPressureTendency, 
		HOURLYPressureChange, 
		HOURLYSeaLevelPressure, 
		HOURLYPrecip, 
		HOURLYAltimeterSetting, 
		DAILYMaximumDryBulbTemp, 
		DAILYMinimumDryBulbTemp, 
		DAILYAverageDryBulbTemp,  
		DAILYDeptFromNormalAverageTemp, 
		DAILYAverageRelativeHumidity, 
		DAILYAverageDewPointTemp, 
		DAILYAverageWetBulbTemp, 
		DAILYHeatingDegreeDays, 
		DAILYCoolingDegreeDays, 
		DAILYSunrise,    // Question1
		DAILYSunset,     // Question1
		DAILYWeather, 
		DAILYPrecip, 
		DAILYSnowfall, 
		DAILYSnowDepth, 
		DAILYAverageStationPressure, 
		DAILYAverageSeaLevelPressure, 
		DAILYAverageWindSpeed, 
		DAILYPeakWindSpeed, 
		PeakWindDirection, 
		DAILYSustainedWindSpeed, 
		DAILYSustainedWindDirection, 
		MonthlyMaximumTemp, 
		MonthlyMinimumTemp, 
		MonthlyMeanTemp, 
		MonthlyAverageRH, 
		MonthlyDewpointTemp, 
		MonthlyWetBulbTemp, 
		MonthlyAvgHeatingDegreeDays, 
		MonthlyAvgCoolingDegreeDays, 
		MonthlyStationPressure, 
		MonthlySeaLevelPressure, 
		MonthlyAverageWindSpeed, 
		MonthlyTotalSnowfall, 
		MonthlyDeptFromNormalMaximumTemp, 
		MonthlyDeptFromNormalMinimumTemp, 
		MonthlyDeptFromNormalAverageTemp, 
		MonthlyDeptFromNormalPrecip, 
		MonthlyTotalLiquidPrecip, 
		MonthlyGreatestPrecip, 
		MonthlyGreatestPrecipDate, 
		MonthlyGreatestSnowfall, 
		MonthlyGreatestSnowfallDate, 
		MonthlyGreatestSnowDepth, 
		MonthlyGreatestSnowDepthDate, 
		MonthlyDaysWithGT90Temp, 
		MonthlyDaysWithLT32Temp, 
		MonthlyDaysWithGT32Temp, 
		MonthlyDaysWithLT0Temp, 
		MonthlyDaysWithGT001Precip, 
		MonthlyDaysWithGT010Precip, 
		MonthlyDaysWithGT1Snow, 
		MonthlyMaxSeaLevelPressureValue, 
		MonthlyMaxSeaLevelPressureDate, 
		MonthlyMaxSeaLevelPressureTime, 
		MonthlyMinSeaLevelPressureValue, 
		MonthlyMinSeaLevelPressureDate, 
		MonthlyMinSeaLevelPressureTime, 
		MonthlyTotalHeatingDegreeDays, 
		MonthlyTotalCoolingDegreeDays, 
		MonthlyDeptFromNormalHeatingDD, 
		MonthlyDeptFromNormalCoolingDD, 
		MonthlyTotalSeasonToDateHeatingDD, 
		MonthlyTotalSeasonToDateCoolingDD

	}
	
	private CSVParser getParser(String filename) throws IOException
	{
		InputStream is = WeatherFileReader.class.getResourceAsStream(filename);
		Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		
		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
		return csvParser;
	}
	
	public void read(String filename) throws IOException, ParseException
	{
		CSVParser csvParser = getParser(filename);
		for (CSVRecord csvRecord : csvParser) {
			// Accessing Values by Column Index
			
			if (csvRecord.getRecordNumber() ==1) // Skip Headers
				continue;
			String stationId = csvRecord.get(HEADERS.STATION.ordinal());
			String stationName = csvRecord.get(HEADERS.STATION_NAME.ordinal());
			String elevation = csvRecord.get(HEADERS.ELEVATION.ordinal());
			String dateString = csvRecord.get(HEADERS.DATE.ordinal());
			
			log.finest(String.format("Record No - %s\n---------------\n"
					+ "StationId : %s\n"
					+ "StationName : %s\n"
					+ "Elevation : %s\n"
					+ "Date : %s\n"
					+ "---------------\n\n", 
					csvRecord.getRecordNumber(),
					stationId,
					stationName,
					elevation,
					Utils.getDate(dateString))
					);
		}
		csvParser.close();
	}
	
	/**
	 * Returns the Average Daytime Temperature in Canadian, TX
	 * for a given date.
	 * 
	 * @param date
	 * @return
	 * @throws IOException 
	 */
	public AverageDaytimeTemperature getAverageDaytimeTemperature(Date date) 
		throws Exception
	{
		AverageDaytimeTemperature results = new AverageDaytimeTemperature(date);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(results.getDate());
		Date start = cal.getTime();
		
		List<Integer> temps = new ArrayList<Integer>();
		
		CSVParser csvParser = getParser(DATA_FILE_CANADIAN_TX);
		for (CSVRecord csvRecord : csvParser) 
		{
			if (csvRecord.getRecordNumber() ==1) // Skip Headers
				continue;
			Date dateCheck = Utils.getDate(csvRecord.get(HEADERS.DATE.ordinal()));	
			Date sunrise = Utils.getDate(dateCheck, csvRecord.get(HEADERS.DAILYSunrise.ordinal()));	
			Date sunset = Utils.getDate(dateCheck, csvRecord.get(HEADERS.DAILYSunset.ordinal()));	
			String stationName = csvRecord.get(HEADERS.STATION_NAME.ordinal());
			
			if (dateCheck.before(start))
				continue;
			
			if (dateCheck.before(sunrise))
				continue;
			
			if (dateCheck.after(sunset))
				break;
			
			String sTemp = csvRecord.get(HEADERS.HOURLYDRYBULBTEMPF.ordinal());
			if (sTemp.trim().length()==0)
				continue;
			if (sTemp.indexOf('s')>0) // remove suspect 's' character.
				sTemp = sTemp.replace("s", "");
			
			int hourlyTemp = Utils.getInt(sTemp);
			results.setSunrise(sunrise);
			results.setSunset(sunset);
			results.setStationName(stationName);
			temps.add(hourlyTemp);
		}
		int averageTemp = Utils.average(temps);
		double stdDevTemp = Utils.stdDev(temps);
		log.finer(String.format("Temps[%s] %s - Average %s", temps, temps.size(), averageTemp));
		results.setAverageTemp(averageTemp);
		results.setStdDevTemp(stdDevTemp);
		return results;
	}
	
	/**
	 * Return the Wind Chill (average, rounded) when temperature is less than 40 degrees
	 * Also includes the complete data set used for averaging.
	 * @param date
	 * @return an object consisting of the Date and averages of Temp, Wind, and WindChill
	 * @throws Exception
	 */
	public WindChillWhenBelow40 getWindChillWhenTempBelow40(Date date) 
			throws Exception
		{
			WindChillWhenBelow40 results = new WindChillWhenBelow40(date);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(results.getDate());
			Date start = cal.getTime();
			cal.add(Calendar.DATE, 1);
			Date end = cal.getTime();
			
			List<Integer> temps = new ArrayList<Integer>();
			List<Integer> chill = new ArrayList<Integer>();
			List<Integer> winds = new ArrayList<Integer>();
			
			CSVParser csvParser = getParser(DATA_FILE_CANADIAN_TX);
			for (CSVRecord csvRecord : csvParser) 
			{
				if (csvRecord.getRecordNumber() ==1) // Skip Headers
					continue;
				Date dateCheck = Utils.getDate(csvRecord.get(HEADERS.DATE.ordinal()));	
				String stationName = csvRecord.get(HEADERS.STATION_NAME.ordinal());
				
				if (dateCheck.before(start))
					continue;
				
				if (dateCheck.after(end))
					break;
				
				String sTemp = csvRecord.get(HEADERS.HOURLYDRYBULBTEMPF.ordinal());
				if (sTemp.trim().length()==0)
					continue;
				if (sTemp.indexOf('s')>0) // remove suspect 's' character.
					sTemp = sTemp.replace("s", "");
				int hourlyTemp = Utils.getInt(sTemp);
				
				if (hourlyTemp >= 40)
					continue;
				
				String sWind = csvRecord.get(HEADERS.HOURLYWindSpeed.ordinal());
				int hourlyWind = Utils.getInt(sWind);
				int windChill = Utils.windChill(hourlyTemp, hourlyWind);
				
				if (hourlyWind < 5)  // if winds are < 5mph, then WindChill == Temp
					windChill = hourlyTemp;
				
				results.setStationName(stationName);
				results.addResults(dateCheck, windChill);
				temps.add(hourlyTemp);
				chill.add(windChill);
				winds.add(hourlyWind);
			}
			int averageTemp = Utils.average(temps);
			int averageWindChill = Utils.average(chill);
			int averageWind = Utils.average(winds);
			results.setAverageTemp(averageTemp);
			results.setAverageWind(averageWind);
			results.setAverageWindChill(averageWindChill);
			return results;
		}	
	
	/**
	 * This needs to return a map of dates rounded to 5 minute increments (ceil) so the data sets
	 * will match.  
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private Map<Date,Integer> getTempsEveryHour(String fileName) throws IOException
	{
		
		Map<Date,Integer> results = new HashMap<Date,Integer>();
		
		CSVParser csvParser = getParser(fileName);
		for (CSVRecord csvRecord : csvParser) 
		{
			if (csvRecord.getRecordNumber() ==1) // Skip Headers
				continue;
			try
			{
				Date dateCheck = Utils.getDateByHour(csvRecord.get(HEADERS.DATE.ordinal()));	
				
				String sTemp = csvRecord.get(HEADERS.HOURLYDRYBULBTEMPF.ordinal());
				if (sTemp.trim().length()==0)
					continue;
				if (sTemp.indexOf('s')>0) // remove suspect 's' character.
					sTemp = sTemp.replace("s", "");
				int hourlyTemp = Utils.getInt(sTemp);
				results.put(dateCheck, hourlyTemp);
			}
			catch(ParseException e)
			{
				log.warning(String.format("Failed to Parse Row; Bad Date %s", csvRecord.get(HEADERS.DATE.ordinal())));
			}
		}
		return results;
	}
	
	private Map<Date, Map<CharSequence,Integer> >  getTempsByDay(Map<Date, Integer> tempMap) 
		throws ParseException 
	{
		
		Map<Date, Map<CharSequence,Integer> > results = new HashMap<Date, Map<CharSequence,Integer> >();
		SimpleDateFormat formatter = new SimpleDateFormat("kk:mm");
		
		for(Date fiveMin : tempMap.keySet())
		{
			Date daily = Utils.getZeroDate(fiveMin);
			if (!results.containsKey(daily))
			{
				results.put(daily, new HashMap<CharSequence,Integer>());
			}
			String chars = formatter.format(fiveMin);
			results.get(daily).put(chars, tempMap.get(fiveMin));
		}
		
		return results;
	}
	
	public MostSimilarDay getMostSimilarDay() 
			throws Exception
	{
		MostSimilarDay results = new MostSimilarDay();
		
		// Get all the Hourly Temperatures
		Map<Date,Integer> texas = getTempsEveryHour(DATA_FILE_CANADIAN_TX);  // Texas Data is 0:15, 0:35, 0:55
		Map<Date,Integer> georgia = getTempsEveryHour(DATA_FILE_WBAN_GA);    // Ga Data is 0:00, 0:52

		// Aggregate Temperatures by Day
		Map<Date, Map<CharSequence,Integer> > texasDaily = getTempsByDay(texas);
		Map<Date, Map<CharSequence,Integer> > georgiaDaily = getTempsByDay(georgia);
		
		CosineSimilarity compare = new CosineSimilarity();
		double similarity = 0.0d;
		
		for (Date key : texasDaily.keySet())
		{
			// Continue if datasets do not contain same day.
			if (!georgiaDaily.containsKey(key))  
				continue;
			Map<CharSequence,Integer> txTemps = texasDaily.get(key);
			Map<CharSequence,Integer> gaTemps = georgiaDaily.get(key);
			
			// Compare the data sets
			double tempresult = compare.cosineSimilarity(txTemps, gaTemps);
			if (tempresult > similarity)
			{
				similarity = tempresult;
				results.setAverageGeorgiaTemp(Utils.average(gaTemps.values()));
				results.setAverageTexasTemp(Utils.average(txTemps.values()));
				results.setDate(key);
				results.setGeorgiaTemps(gaTemps.values());
				results.setTexasTemps(txTemps.values());
				results.setSimilarity(similarity);
			}
			
			if (similarity == 1.0d)  // Can't get anybetter
				break;
		}
		
		return results;
	}

}
