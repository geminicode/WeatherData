package edu.gtri.weather;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.logging.Logger;

import org.junit.Test;

public class WeatherFileReaderTest {

	Logger log = Logger.getLogger(getClass().getName());
	@Test
	public void testNotNull()
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
	}
	
	@Test
	public void testRead() throws IOException, ParseException
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
		reader.read(WeatherFileReader.DATA_FILE_CANADIAN_TX);
	}

	@Test
	public void testRead2() throws IOException, ParseException
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
		reader.read(WeatherFileReader.DATA_FILE_WBAN_GA);
	}

	@Test
	public void testGetAverageDaytimeTemperature() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
		Calendar cal = Calendar.getInstance();
		cal.set(2017,Calendar.FEBRUARY,1);
		AverageDaytimeTemperature results = reader.getAverageDaytimeTemperature(cal.getTime());
		assertEquals(43, results.getAverageTemp());
		log.info(results.toString());
	}
	
	@Test
	public void testGetWindChillWhenTempBelow40() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
		Calendar cal = Calendar.getInstance();
		cal.set(2017,Calendar.FEBRUARY,1);
		WindChillWhenBelow40 results = reader.getWindChillWhenTempBelow40(cal.getTime());
		assertEquals(44, results.getResults().size());
		assertEquals(33, results.getAverageTemp());
		log.info(results.toString());
	}	
	
	@Test
	public void testGetMostSimilarDay() throws Exception
	{
		WeatherFileReader reader = new WeatherFileReader();
		assertNotNull(reader);
		MostSimilarDay results = reader.getMostSimilarDay();
		log.info(results.toString());
	}	
	
}
